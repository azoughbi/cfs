from django.db import models
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.dispatch import dispatcher
from django.db.models import signals
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser,PermissionsMixin
)
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
main_role=""
content_role_type=""
content_city_type=""
dont_send_counter=""


class City(models.Model):
    name = models.CharField(max_length=32)

    def __unicode__(self):
        return self.name


class Content(models.Model):
    name = models.CharField(max_length=32)
    content = models.TextField()
    roles_type=models.CharField(max_length=32,editable=False)
    city_type=models.CharField(max_length=32,editable=False)
    Send = models.CharField(max_length=50, choices=(("send to Area Supervisor","Send to Area Supervisor"),("Do not send","Do not Send")))
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        permissions = (
            ('View_content', 'View content'),
        )
    def __unicode__(self):
        #print "abhi"
        #print self.Send
        return self.name

    def __init__(self,  *args, **kwargs):
        global main_role
        #print main_role
        super(Content, self).__init__(*args, **kwargs)
        #print self._meta.get_field_by_name('choices_f')[0]._choices
        if main_role=="is_Enumerator":
            self._meta.get_field_by_name('Send')[0]._choices=(("send to Area Supervisor","Send to Area Supervisor"),("Do not send","Do not Send"))
        elif main_role=="is_AreaSupervisor":
            self._meta.get_field_by_name('Send')[0]._choices=(("send to Enumerator","Send to Enumerator"),("send to Verification","Send to Verification"),("Do not send","Do not Send"))
        elif main_role=="is_Verification":
            self._meta.get_field_by_name('Send')[0]._choices=(("send to Area Supervisor","Send to Area Supervisor"),("Do not send","Do not Send"))
        elif main_role=="is_admin":
            self._meta.get_field_by_name('Send')[0]._choices=(("send to Area Supervisor","Send to Area Supervisor"),("send to Verification","Send to Verification"),("send to Enumerator","Send to Enumerator"),("Do not send","Do not Send"))


    @classmethod
    def from_db(cls, db, field_names, values):
        #print "from_db"
        #print Send
        #print "enter in from_db"
        #print cls
        #print db.name
        #print field_names
        #print values
        dont_send_counter=main_role
        global main_role
        global content_role_type
        global content_city_type
        #print main_role
    # default implementation of from_db() (could be replaced
    # with super())
        if cls._deferred:
            instance = cls(**zip(field_names, values))
        else:
            #print "from_db"
            #print main_role
            if main_role=="is_admin":
                instance = cls(*values)
            elif main_role=="is_Enumerator":
                if cls(*values).Send=="send to Enumerator" or cls(*values).Send=="is_Enumerator":
                    if cls(*values).roles_type==content_role_type:
                        instance = cls(*values)
                    else:
                        instance = cls(values)
                else:
                    instance = cls(values)
            elif main_role=="is_AreaSupervisor":
                if cls(*values).Send=="send to Area Supervisor" or cls(*values).Send=="is_AreaSupervisor":
                    if content_city_type==cls(*values).city_type:
                        instance = cls(*values)
                    else:
                        instance = cls(values)
                else:
                    instance = cls(values)
            elif main_role=="is_Verification":
                if cls(*values).Send=="send to Verification" or cls(*values).Send=="is_Verification":
                    if content_city_type==cls(*values).city_type:
                        instance = cls(*values)
                    else:
                        instance = cls(values)
                else:
                    instance = cls(values)
        #instance._state.adding = False
        #instance._state.db = db
        # customization to store the original field values on the instance
        #instance._loaded_values = dict(zip(field_names, values))
        return instance

    def clean(self):
        global main_role
        #print main_role
        # Don't allow draft entries to have a pub_date.
        if (main_role == "is_Enumerator" and self.Send == "send to Verification") or (main_role == "is_Enumerator" and self.Send == "send to Enumerator"):
            raise ValidationError(_('You are Enumerator Please send the request to Area Supervisor only.'))
        elif main_role == "is_AreaSupervisor" and self.Send == "send to Area Supervisor":
            raise ValidationError(_('You are Area Supervisor Please send the request to Verification and Enumerator only.'))
        elif (main_role == "is_Verification" and self.Send == "send to Verification") or (main_role == "is_Verification" and self.Send == "send to Enumerator"):
            raise ValidationError(_('You are Verification Please send the request to Area Supervisor only.'))

    def save(self, *args, **kwargs):
        global content_role_type
        global main_role
        global content_city_type
        if main_role=="is_Enumerator" or main_role=="is_admin":
            self.roles_type=content_role_type
            self.city_type=content_city_type
            if self.Send=="Do not send":
                self.Send=main_role
            else:
                pass

        elif main_role=="is_AreaSupervisor" or main_role=="is_admin":
            self.roles_type==content_role_type
            self.city_type=content_city_type
            if self.Send=="Do not send":
                self.Send=main_role
            else:
                pass

        elif main_role=="is_Verification" or main_role=="is_admin":
            self.roles_type==content_role_type
            self.city_type=content_city_type
            if self.Send=="Do not send":
                self.Send=main_role
            else:
                pass

        """if not self._state.adding and (
                self.creator_id != self._loaded_values['creator_id']):
            raise ValueError("Updating the value of creator isn't allowed")"""
        super(Content, self).save(*args, **kwargs)


class MyUserManager(BaseUserManager):
    def create_user(self,username,city,password=None):
        #print "createusers"
        """
        Creates and saves a User with the given email, date ofs
        birth and password.
        """
        if not username:
            raise ValueError('Users must have an username')

        user = self.model(
            #email=self.normalize_email(email),
            username=username,
            city_id=city,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self,username,city,password):
        #print city
        #print "supersuers"
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(username,
            password=password,
            city=city
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser,PermissionsMixin):
    #global main_role=""
    #print "Myuser"
    #MyUser.dont_send_counter=""
    username = models.CharField(
        verbose_name='UserName',
        max_length=255,
        unique=True,
    )

    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    city=models.ForeignKey(City)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_Enumerator= models.BooleanField(default=False)
    is_AreaSupervisor= models.BooleanField(default=False)
    is_Verification= models.BooleanField(default=False)
    objects = MyUserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['city',]

    def get_full_name(self):
        # The user is identified by their email address
        return self.username

    def get_short_name(self):
        # The user is identified by their email address
        return self.username

    def __str__(self):              # __unicode__ on Python 2
        return self.username

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        global main_role
        global content_city_type
        global dont_send_counter
        global content_role_type
        content_role_type=self.email
        content_city_type=str(self.city_id)
        #print "is staff"
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        if self.is_Enumerator==True:
            dont_send_counter="enumeratordontsend"
            main_role="is_Enumerator"
            return self.is_Enumerator
        elif self.is_AreaSupervisor==True:
            dont_send_counter="areasupervisordontsend"
            main_role="is_AreaSupervisor"
            return self.is_AreaSupervisor
        elif self.is_Verification==True:
            dont_send_counter="verificationdontsend"
            main_role="is_Verification"
            return self.is_Verification
        else:
            dont_send_counter="admindontsend"
            main_role="is_admin"
            return self.is_admin
    """class Meta:
        permissions = (("can_view_article", "Can view article"),
                       ("can_add_article", "Can add article"),("can_edit_article", "Can edit article"),("can_delete_article", "Can delete article"),)"""
    """@property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_Enumerator"""

#register_custom_permissions_simple((("is_Area Supervisor", "User is Area Supervisor"),))